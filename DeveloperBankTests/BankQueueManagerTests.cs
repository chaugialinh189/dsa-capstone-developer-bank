﻿public class BankQueueManagerTests
{
    [Fact]
    public void AddCustomerToTheQueue_AddsCustomerToQueue()
    {
        // Arrange
        int maxEconomyQueue = 10;
        int maxVIPQueue = 10;
        var bankQueueManager = new BankQueueManager(maxEconomyQueue, maxVIPQueue);
        var expectedQueueCount = 1;
        var expectedVIPQueueCount = 0;
        var customer = new Customer { Name = "John", WithdrawalAmount = 100_000_000, OrderNumber = 1 };

        // Act
        bankQueueManager.AddCustomerToTheQueue(customer);

        // Assert
        Assert.Equal(expectedQueueCount, bankQueueManager.EconomyQueueCount);
        Assert.Equal(expectedVIPQueueCount, bankQueueManager.VIPQueueCount);
    }

    [Fact]
    public void AddCustomerToTheQueue_AddsVIPCustomerToQueue()
    {
        // Arrange
        int maxEconomyQueue = 10;
        int maxVIPQueue = 10;
        var bankQueueManager = new BankQueueManager(maxEconomyQueue, maxVIPQueue);
        var expectedVIPQueueCount = 1;
        var vipCustomer = new Customer { Name = "John", WithdrawalAmount = 600_000_000, OrderNumber = 2 };

        // Act
        bankQueueManager.AddCustomerToTheQueue(vipCustomer);

        // Assert
        Assert.Equal(expectedVIPQueueCount, bankQueueManager.VIPQueueCount);
    }


    [Fact]
    public void AddCustomerToTheQueue_ShouldThrowException_WhenVIPQueueFull()
    {
        // Arrange
        int maxEconomyQueue = 3;
        int maxVIPQueue = 3;
        var bankQueueManager = new BankQueueManager(maxEconomyQueue, maxVIPQueue);
        var vipCustomer1 = new Customer { Name = "John 1", WithdrawalAmount = 600_000_000, OrderNumber = 1 };
        var vipCustomer2 = new Customer { Name = "John 2", WithdrawalAmount = 700_000_000, OrderNumber = 2 };
        var vipCustomer3 = new Customer { Name = "John 3", WithdrawalAmount = 800_000_000, OrderNumber = 3 };
        var vipCustomer4 = new Customer { Name = "John 4", WithdrawalAmount = 900_000_000, OrderNumber = 4 };
        
        bankQueueManager.AddCustomerToTheQueue(vipCustomer1);
        bankQueueManager.AddCustomerToTheQueue(vipCustomer2);
        bankQueueManager.AddCustomerToTheQueue(vipCustomer3);

        // Act & Assert
        Assert.Throws<InvalidOperationException>(() =>
        {
            bankQueueManager.AddCustomerToTheQueue(vipCustomer4);
        });
    }

    [Fact]
    public void AddCustomerToTheQueue_ShouldThrowException_WhenEconomyQueueFull()
    {
        // Arrange
        int maxEconomyQueue = 3;
        int maxVIPQueue = 3;
        var bankQueueManager = new BankQueueManager(maxEconomyQueue, maxVIPQueue);
        var customer1 = new Customer { Name = "John 1", WithdrawalAmount = 100_000_000, OrderNumber = 1 };
        var customer2 = new Customer { Name = "John 2", WithdrawalAmount = 200_000_000, OrderNumber = 2 };
        var customer3 = new Customer { Name = "John 3", WithdrawalAmount = 300_000_000, OrderNumber = 3 };
        var customer4 = new Customer { Name = "John 4", WithdrawalAmount = 400_000_000, OrderNumber = 4 };

        bankQueueManager.AddCustomerToTheQueue(customer1);
        bankQueueManager.AddCustomerToTheQueue(customer2);
        bankQueueManager.AddCustomerToTheQueue(customer3);

        // Act & Assert
        // TODO: Throw InvalidOperationException when call bankQueueManager.AddCustomerToTheQueue(customer4);
    }


    [Fact]
    public void GetNextVIPCustomer_ShouldGetVIPCustomer_WhenCustomerIsInTheHeadQueue()
    {
        // Arrange
        int maxEconomyQueue = 3;
        int maxVIPQueue = 3;
        var bankQueueManager = new BankQueueManager(maxEconomyQueue, maxVIPQueue);
        var vipCustomer1 = new Customer { Name = "John 1", WithdrawalAmount = 600_000_000, OrderNumber = 1 };
        var vipCustomer2 = new Customer { Name = "John 2", WithdrawalAmount = 700_000_000, OrderNumber = 2 };
        var vipCustomer3 = new Customer { Name = "John 3", WithdrawalAmount = 800_000_000, OrderNumber = 3 };

        bankQueueManager.AddCustomerToTheQueue(vipCustomer1);
        bankQueueManager.AddCustomerToTheQueue(vipCustomer2);
        bankQueueManager.AddCustomerToTheQueue(vipCustomer3);

        // Act
        var actual = bankQueueManager.GetNextVIPCustomer();

        // TODO: assert VIP customer
    }


    [Fact]
    public void GetNextVIPCustomer_ShouldThrowException_WhenQueueEmpty()
    {
        // Arrange
        int maxEconomyQueue = 3;
        int maxVIPQueue = 3;
        var bankQueueManager = new BankQueueManager(maxEconomyQueue, maxVIPQueue);
        
        // Act
        var actual = bankQueueManager.GetNextVIPCustomer();

        // TODO: assert throw exception
    }


    [Fact]
    public void GetNextEconomyCustomer_ShouldReturnCustomer_WhenCustomerIsInTheHeadQueue()
    {
        // Arrange
        int maxEconomyQueue = 3;
        int maxVIPQueue = 3;
        var bankQueueManager = new BankQueueManager(maxEconomyQueue, maxVIPQueue);
        var customer1 = new Customer { Name = "John 1", WithdrawalAmount = 100_000_000, OrderNumber = 1 };
        var customer2 = new Customer { Name = "John 2", WithdrawalAmount = 200_000_000, OrderNumber = 2 };
        var customer3 = new Customer { Name = "John 3", WithdrawalAmount = 300_000_000, OrderNumber = 3 };

        bankQueueManager.AddCustomerToTheQueue(customer1);
        bankQueueManager.AddCustomerToTheQueue(customer2);
        bankQueueManager.AddCustomerToTheQueue(customer3);

        // Act
        var actual = bankQueueManager.GetNextEconomyCustomer();

        // TODO: assert economy customer
    }


    [Fact]
    public void GetNextEconomyCustomer_ShouldThrowException_WhenQueueEmpty()
    {
        // Arrange
        int maxEconomyQueue = 3;
        int maxVIPQueue = 3;
        var bankQueueManager = new BankQueueManager(maxEconomyQueue, maxVIPQueue);

        // Act
        var actual = bankQueueManager.GetNextEconomyCustomer();

        // TODO: assert throw exception
    }

    [Fact]
    public void GetNextReadyCustomer_ShouldReturnCustomer()
    {
        // Arrange
        int maxEconomyQueue = 3;
        int maxVIPQueue = 3;
        var bankQueueManager = new BankQueueManager(maxEconomyQueue, maxVIPQueue);
        var customer1 = new Customer { Name = "John 1", WithdrawalAmount = 100_000_000, OrderNumber = 1 };
        var customer2 = new Customer { Name = "John 2", WithdrawalAmount = 200_000_000, OrderNumber = 2 };
        var customer3 = new Customer { Name = "John 3", WithdrawalAmount = 300_000_000, OrderNumber = 3 };

        bankQueueManager.AddCustomerToTheQueue(customer1);
        bankQueueManager.AddCustomerToTheQueue(customer2);
        bankQueueManager.AddCustomerToTheQueue(customer3);

        // Act
        var actual = bankQueueManager.GetNextReadyCustomer();

        // TODO: assert NextReadyCustomer
    }



    [Fact]
    public void ReportSystem_ShouldReturnCorrectValue()
    {
        // TODO: Arrange
        int maxEconomyQueue = 3;
        int maxVIPQueue = 3;
        var bankQueueManager = new BankQueueManager(maxEconomyQueue, maxVIPQueue);

        // Act
        var actual = bankQueueManager.ReportSystem();

        // TODO: assert NextReadyCustomer
    }

}