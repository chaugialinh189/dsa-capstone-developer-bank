﻿public class BankProgram
{
    public BankQueueManager bankManager;

    public BankProgram()
    {
        int maxEconomyQueue = 10;
        int maxVIPQueue = 10;
        bankManager = new BankQueueManager(maxEconomyQueue, maxVIPQueue);
    }

    public void DisplayMenu()
    {
        Console.WriteLine("----------- Quản lý Hàng đợi Ngân hàng -----------");
        Console.WriteLine("1. Thêm khách hàng vào hàng đợi");
        Console.WriteLine("2. Gọi tên khách hàng VIP tiếp theo");
        Console.WriteLine("3. Gọi tên khách hàng Thường tiếp theo");
        Console.WriteLine("4. Hiển thị khách hàng sắp tới");
        Console.WriteLine("5. Thống kê");
        Console.WriteLine("6. Thoát chương trình");
        Console.Write("Nhập lựa chọn của bạn: ");
    }

    public void Start()
    {
        while (true)
        {
            DisplayMenu();

            string choice = Console.ReadLine();
            Console.WriteLine();

            switch (choice)
            {
                case "1":
                    // TODO: Thêm khách hàng vào hàng đợi
                    bankManager.AddCustomerToTheQueue(new Customer());
                    break;
                case "2":
                    // TODO: Gọi tên khách hàng VIP tiếp theo
                    var nextVIPCustomer = bankManager.GetNextVIPCustomer();
                    break;
                case "3":
                    // TODO: Gọi tên khách hàng Thường tiếp theo
                    var nextCustomer = bankManager.GetNextEconomyCustomer();
                    break;
                case "4":
                    // TODO: Hiển thị khách hàng sắp tới
                    bankManager.GetNextReadyCustomer();
                    break;
                case "5":
                    // TODO: Thống kê
                    bankManager.ReportSystem();
                    break;
                case "6":
                    // TODO: Lưu trạng thái hàng đợi vào tệp (nếu cần)
                    Environment.Exit(0);
                    break;
                default:
                    Console.WriteLine("Lựa chọn không hợp lệ. Vui lòng nhập một lựa chọn hợp lệ.");
                    break;
            }

            Console.WriteLine();
        }
    }

}

